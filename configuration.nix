{ config, pkgs, lib, ... }:

{
  imports = [ ./core ];
  nix.package = pkgs.nixFlakes;
  nix.extraOptions = "extra-experimental-features = nix-command flakes";
  system.stateVersion = "21.11";
}
