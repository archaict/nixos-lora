{ config, lib, pkgs, ... }:

let

  user = "lora";
  password = "nixos_lora";

in {

  users = {
    mutableUsers = false;
    users."${user}" = {
      isNormalUser = true;
      password = password;
      extraGroups = [ "wheel" "networkmanager" "audio" "video" "sound" "input" "tty" "lora" "docker" ];
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCur/NUyJkO6SMsFzbdPFezN+OP/JHxHvt55Hx99j1umrbfBtm36WgEV+ECgwKNulTYpZ1Hmd/4zdkr/eamZlsjOOb2OYROOnw315acsBPQPDki+6fch8WXrALxRaE3Vc4IMzJjDrlJvA7M8D9BzQvZv5n8hjkrWUBQy6XwXUheGCuCxThrfsR1DnNgIgm45XKp5ZAotbpu1iyCqldRiOf2i6cmMbLz6f+xraCf05XhNEaeaOua/p+bcRqUboo97PG2zW1B1FrNe+t7kaJ1AeZIqyL5iVO3L011Ks0R0u/mTHkUWtE1+0F7nFl3esIlG9p1xIu4WNyor3njQtaCi/B75/xGJEFHE6dUbZfdy9LD64BxbQ9zvNmyh5hcE/oZtuKxiYtHFcRXkaWgGQTB+RJ7zsd7WVsnVwq7g6joM7Xw9nJgteaLqEc1JpZp+mtZk/jYM0k2L2iOIfBJCz2PUuJsB0ccM456gPh8Z2aZwJhyG1B0th5PGyOxBvVC67rSUWM= archaict"
      ];
    };
  };

}
