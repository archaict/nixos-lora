{ config, lib, pkgs, ... }:

{


  environment.systemPackages = with pkgs; [
    endlessh
  ];

  # IPTABLES
  networking.firewall.extraCommands = ''

  # Block Invalid Packages
  iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP

  # Block Packets [not SYN]
  iptables -t mangle -A PREROUTING -p tcp ! --syn -m conntrack --ctstate NEW -j DROP

  # Block Uncommon MSS VALUES
  iptables -t mangle -A PREROUTING -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP

  # Block Bogus TCP Flags
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,ACK FIN -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,URG URG -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,PSH PSH -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL NONE -j DROP

  # Block packets from spoofing
  iptables -t mangle -A PREROUTING -s 224.0.0.0/3 -j DROP
  iptables -t mangle -A PREROUTING -s 169.254.0.0/16 -j DROP
  iptables -t mangle -A PREROUTING -s 172.16.0.0/12 -j DROP
  iptables -t mangle -A PREROUTING -s 192.0.2.0/24 -j DROP
# iptables -t mangle -A PREROUTING -s 192.168.0.0/16 -j DROP
  iptables -t mangle -A PREROUTING -s 10.0.0.0/8 -j DROP
  iptables -t mangle -A PREROUTING -s 0.0.0.0/8 -j DROP
  iptables -t mangle -A PREROUTING -s 240.0.0.0/5 -j DROP
  iptables -t mangle -A PREROUTING -s 127.0.0.0/8 ! -i lo -j DROP


   # Drops ICMP | Fragmentation Flood
    iptables -t mangle -A PREROUTING -p icmp -j DROP


  # Connection Attacks
  iptables -A INPUT -p tcp -m connlimit --connlimit-above 80 -j REJECT --reject-with tcp-reset

  # Limit Connection per Second
  iptables -A INPUT -p tcp -m conntrack --ctstate NEW -m limit --limit 60/s --limit-burst 20 -j ACCEPT
  iptables -A INPUT -p tcp -m conntrack --ctstate NEW -j DROP

  # Rule Blocks Fragmented Packets
  iptables -t mangle -A PREROUTING -f -j DROP

  # Limit RST Flagged connection
  iptables -A INPUT -p tcp --tcp-flags RST RST -m limit --limit 2/s --limit-burst 2 -j ACCEPT
  iptables -A INPUT -p tcp --tcp-flags RST RST -j DROP

  # SYNPROXY
# iptables -t raw -A PREROUTING -p tcp -m tcp --syn -j CT --notrack
# iptables -A INPUT -p tcp -m tcp -m conntrack --ctstate INVALID,UNTRACKED -j SYNPROXY --sack-perm --timestamp --wscale 7 --mss 1460
# iptables -A INPUT -m conntrack --ctstate INVALID -j DROP

  # SSH brute-force protection
# iptables -A INPUT -p tcp --dport ssh -m conntrack --ctstate NEW -m recent --set
# iptables -A INPUT -p tcp --dport ssh -m conntrack --ctstate NEW -m recent --update --seconds 60 --hitcount 10 -j DROP

  # Port Scans Mitigation
  iptables -N port-scanning
# iptables -A port-scanning -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s --limit-burst 2 -j RETURN
# iptables -A port-scanning -j DROP

    '';

}
