{ config, lib, pkgs, ... }:

let hostname = "nixos-lora";

in {

  imports = [
    ./security.nix
    ./ssh.nix
  ];

  # Required for wireless
  hardware.enableRedistributableFirmware = true;

  networking = {

    hostName = hostname;
    wireless = {
      enable = false;
      # Use environmentFile for Secrets
      environmentFile = "/etc/nixos/.secret/wireless.env";
      networks.Core.psk = "@HOME_PSK@";
      networks.archaict.psk = "@ARCH_PSK@";
      interfaces = [ "wlan0" ];
    };

    firewall = {
      enable = true;
      allowedTCPPorts = [ 80 443 9090 ];
    };

  };

  boot.kernel.sysctl = { "net.ipv4.conf.all.forwarding" = true; };

  services.httpd = {
    enable = true;
    adminAddr = "example@example.org";
  };

  networking.networkmanager.enable = true;

}
