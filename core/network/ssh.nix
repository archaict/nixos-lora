{ config, lib, pkgs, ... }:

{
  services.openssh = {
    enable = true;
    banner = "NixOS LoRaWAN Server";
    passwordAuthentication = true;
    permitRootLogin = "no";
    ports = [ 9090 ];
  };
}
