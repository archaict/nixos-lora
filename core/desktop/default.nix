{ config, lib, pkgs, ... }:

{
  services.xserver = {
    # Turn this off if not needed
    enable = true;
    displayManager.lightdm.enable = true;
    desktopManager.xfce.enable = true;
  };
}
