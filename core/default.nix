{ config, lib, pkgs, ... }:

{

  imports = [
    ./user
    ./network
    ./packages
    ./desktop
    ./hardware
    ./nixopts
  ];

}
