{ config, lib, pkgs, ... }:

{

  imports = [
    ./bash.nix
    ./ids.nix
    ./virtualization.nix
  ];

  nixpkgs.config = { allowUnfree = true; };

  environment.systemPackages = with pkgs; [

    firefox

    # editor
    neovim
    ripgrep

    # for nix language format
    rnix-lsp
    nixfmt

    htop

    # repos
    git

    nodejs

  ];

}
