{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [ snort nmap dig ];
}
