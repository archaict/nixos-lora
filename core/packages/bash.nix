{ config, lib, pkgs, ... }:

{
  programs.bash = {
    shellAliases = {
      "i" = "nixos-rebuild -v switch --flake /etc/nixos --use-remote-sudo";
      "gpi" = "cd /etc/nixos && git pull && sudo nixos-rebuild switch";
      "gpl" = "cd /etc/nixos && git pull";
      "gcm" = "cd /etc/nixos && git commit -m ";
      "gps" = "cd /etc/nixos && git push";
      "chirpy" = "cd /etc/nixos/core/chirpstack && sudo docker-compose up";
    };
  };
}
