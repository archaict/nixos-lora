{ config, lib, pkgs, flake, ... }:

{

  imports = [
    # WARN: This is for hardware-configuration
    # "${
    #   fetchTarball
    #   "https://github.com/NixOS/nixos-hardware/archive/936e4649098d6a5e0762058cb7687be1b2d90550.tar.gz"
    # }/raspberry-pi/4"
    ./audio.nix
  ];

  # Enable filesystem after initial installation
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = [ "noatime" ];
    };
  };

  # Enable GPU acceleration
  hardware.raspberry-pi."4".fkms-3d.enable = true;

  # Save power using ondemand cpu-governor
  powerManagement.cpuFreqGovernor = "ondemand";

}
