{
  description = "Arkiv Configuration Files";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11";
    unstable.url = "github:NixOS/nixpkgs/8e037045fe100b1e965b880d1544d2070e928039";
    flake-utils.url = "github:numtide/flake-utils/flatten-tree-system";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
  };

  outputs =
    { self, nixpkgs, nixos-hardware, unstable, flake-utils, ... }@inputs:

    {

      nixosConfigurations."nixos-lora" = inputs.nixpkgs.lib.nixosSystem {

        system = "aarch64-linux";

        modules = [

          nixos-hardware.nixosModules.raspberry-pi-4
          ./configuration.nix

          {
            nix.registry.nixpkgs.flake = inputs.nixpkgs;
            nixpkgs.overlays = [

              (final: prev: {
                unstable = import inputs.unstable {
                  system = "aarch64-linux";
                  config.allowUnfree = true;
                };
              })

            ];
          }

        ];

        specialArgs = { inherit inputs; };

      };

    };
}
